﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using IOF.XML.V3;

namespace OW.ResultAnalyser.Summary {
  public class Worker {
    private readonly Configuration _Configuration;

    public const string RealDateFormat = "yyyy-MM-dd";

    public Worker(Configuration configuration) {
      _Configuration = configuration;
    }

    public void Work() {
      var events = ProcessEvents(false).OrderBy(p => p.Date).ToList();

      CreateOrganisersClub(events, "organising-clubs.csv");

      var clubMappings = LoadMappings();
      var suffix = "";

      if (_Configuration.FilterOrganisers) {
        events = events.Where(p => _Configuration.Organisers.Any(o => p.Organisers == o)).ToList();
        suffix += "-[organiser-filtered]";
      }

      var runners = events.SelectMany(p => p.Runners, (e, p) => new FlattendRecord { Date = e.Date, EventName = e.Name, Organisers = e.Organisers, FirstName = p.FirstName, LastName = p.LastName, Card = p.Card.CovertToInt(), OriginalClub = p.Club, Club = MapClub(clubMappings, p.Club), Class = p.Class }).ToList();

      if (_Configuration.FilterClubs) {
        runners = runners.Where(p => _Configuration.Clubs.Any(o => p.Club == o || p.Club2 == o)).ToList();
        suffix += "-[club-filtered]";
      }

      if (_Configuration.FilterOriginalClubs) {
        runners = runners.Where(p => _Configuration.OriginalClubs.Any(o => p.OriginalClub == o)).ToList();
        suffix += "-[original-club-filtered]";
      }

      if (_Configuration.FilterSportIdents) {
        runners = runners.Where(p => _Configuration.SportIdents.Any(o => p.Card.HasValue && p.Card.Value == o)).ToList();
        suffix += "-[si-filtered]";
      }

      CreateUnmapperClubs(runners, $"unmapped-clubs{suffix}.csv");
      CreateClubReport(runners, $"runner-clubs{suffix}.csv");
      CreateOriginalClubReport(runners, $"runner-original-clubs{suffix}.csv");
      CreateReport(events, $"report{suffix}.csv");
      CreateRunnerReport(runners, $"runners{suffix}.csv");
      CreatePersonReport(runners, $"person{suffix}.csv");
      CreateCardReport(runners, $"card{suffix}.csv");
      CreateCardPredictReport(runners, $"cardpredict-usage{suffix}.csv", p => p.OrderByDescending(r => r.NumberOfUses).ToList(), p => p.Where(r => r.Date.HasValue && DateTime.Now.AddYears(_Configuration.Year) < r.Date.Value).ToList());
      CreateCardPredictReport(runners, $"cardpredict-alphabetical{suffix}.csv", p => p.OrderBy(r => r.LastName).ThenBy(r => r.FirstName).ToList(), p => p.Where(r => r.Date.HasValue && DateTime.Now.AddYears(_Configuration.Year) < r.Date.Value && !String.IsNullOrWhiteSpace(r.FirstName) && !String.IsNullOrWhiteSpace(r.LastName)).ToList());

      foreach (var person in _Configuration.Persons ?? new List<Person>()) {
        CreateSinglePersonReport(runners, $"{person.FirstName}-{person.LastName}{suffix}.csv".Replace(" ", "-"), p => p.FirstName == person.FirstName && p.LastName == person.LastName);
      }

      CreateRentalReport(runners, $"rental{suffix}.csv");
      CreateCardTypeReport(runners, $"card-type{suffix}.csv");
      CreateCardUsedDateReport(runners, $"card-last-used-date{suffix}.csv");
      CreateCardUsedByEventReport(runners, $"card-used-event{suffix}.csv");
      CreateFlatReport(runners, $"flat{suffix}.csv");

      Console.WriteLine("Done");
      Console.ReadLine();
    }

    private void CreateUnmapperClubs(List<FlattendRecord> all, string fileName) {
      var runners = all.Where(p => String.IsNullOrWhiteSpace(p.Club)).GroupBy(p => p.OriginalClub).Select(p => new { Count = p.Count(), Name = p.Key }).OrderBy(p => p.Name);

      var csv = new CSV();

      csv.Add("Number Of Times", "Name");

      foreach (var r in runners) {
        csv.Add(r.Count, r.Name);
      }

      Write(fileName, csv.Flush());
    }

    private void CreateFlatReport(List<FlattendRecord> all, string fileName) {
      var csv = new CSV();
      csv.Add("Date", "EventName", "Organisers", "FirstName", "LastName", "Club", "Club2", "OriginalClub", "Card", "Class");

      foreach (var r in all) {
        csv.Add(r.Date, r.EventName, r.Organisers, r.FirstName, r.LastName, r.Club, r.Club2, r.OriginalClub, r.Card, r.Class);
      }

      Write(fileName, csv.Flush());
    }

    private void CreateCardUsedByEventReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => $"{p.Date} - {p.EventName}")
        .Select(p => new { First = p.FirstOrDefault(), Cards = p.Select(r => r.Card).Distinct().OrderBy(p => p) })
        .OrderByDescending(p => p.First.Date);

      var csv = new CSV();

      csv.Add("Event Date", "Event Name", "Number Of Cards", "Cards");

      foreach (var r in runners) {
        csv.Add(r.First.Date, r.First.EventName, r.Cards.Count(), String.Join(",", r.Cards.Select(p => $"[{p}]")));
      }

      Write(fileName, csv.Flush());
    }

    private void CreateCardUsedDateReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => $"{p.Card}")
        .Select(p => new { Card = p.Key, NumberOfUses = p.Count(), LastUsed = p.OrderByDescending(c => c.Date).FirstOrDefault(), UsedAt = p.Select(r => new { r.FirstName, r.LastName, r.Club, r.Date }).OrderByDescending(p => p.Date) })
        .OrderByDescending(p => p.Card);

      var csv = new CSV();

      csv.Add("Card", "NumberOfUses", "Last Used", "Last Used By FirstName", "Last Used By LastName", "All Uses");

      foreach (var r in runners) {
        csv.Add(r.Card, r.NumberOfUses, r.LastUsed.Date.ToString(RealDateFormat), r.LastUsed.FirstName, r.LastUsed.LastName, String.Join(",", r.UsedAt.Select(p => $"{p.Date.ToString(RealDateFormat)} {p.FirstName} {p.LastName}")));
      }

      Write(fileName, csv.Flush());
    }

    private void CreateCardTypeReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => p.Card)
        .Select(p => new { Card = p.Key, CardVersion = p.Key.GetCardVersion(), CardUsageCount = p.Count(), Persons = p.GroupBy(g => $"{g.FirstName} {g.LastName}").Select(p => new { Name = p.Key, Count = p.Count() }).OrderByDescending(p => p.Count) })
        .OrderBy(p => p.Card);

      var csv = new CSV();
      csv.Add("Card", "Number of uses", "Names");

      foreach (var r in runners) {
        csv.Add(r.Card, r.CardUsageCount, r.CardVersion, String.Join(",", r.Persons.Select(p => $"{p.Name}[{p.Count}]")));
      }

      Write(fileName, csv.Flush());
    }

    private void CreateRentalReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => p.Card)
        .Select(p => new { Card = p.Key, Person = p.GroupBy(g => $"{g.FirstName} {g.LastName}").Select(p => new { Name = p.Key, Count = p.Count() }).OrderByDescending(p => p.Count), CardUsageCount = p.Count() })
        .OrderBy(p => p.Card);

      var csv = new CSV();
      csv.Add("Card", "Number of uses", "Names");

      foreach (var r in runners) {
        if (r.CardUsageCount > _Configuration.RentalUsageLimit && r.Person.Count() > _Configuration.RentalPersonLimit && r.Person.FirstOrDefault().Count < _Configuration.RentalUsageLimit / 2) {
          csv.Add(r.Card, r.CardUsageCount, String.Join(",", r.Person.Select(p => $"{p.Name}[{p.Count}]")));
        }
      }

      Write(fileName, csv.Flush());
    }

    private void CreateOrganisersClub(List<EventInformation> events, string fileName) {
      var csv = new CSV();
      csv.Add("Name");

      foreach (var e in events.Select(p => p.Organisers).Distinct().OrderBy(p => p)) {
        csv.Add(e);
      }

      Write(fileName, csv.Flush());
    }

    private Dictionary<string, string> LoadMappings() {
      string statsPath = Path.Combine(_Configuration.FileBasePath, "clubs.json");
      var mappings = JsonSerializer.Deserialize<List<ClubMapping>>(File.ReadAllText(statsPath), Helper.GetJsonOptions());
      var returnValue = new Dictionary<string, string>();

      foreach (var m in mappings) {
        foreach (var a in m.Aliases) {
          returnValue.Add(a, m.Name);
        }
      }

      return returnValue;
    }

    private string MapClub(Dictionary<string, string> mappings, string club) {
      if (String.IsNullOrWhiteSpace(club)) {
        return "";
      }

      return mappings.ContainsKey(club) ? mappings[club] : "";
    }

    private void CreateClubReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => p.Club)
        .Select(p => new { p.FirstOrDefault().Club, Count = p.Count() })
        .OrderByDescending(p => p.Count);

      var csv = new CSV();
      csv.Add("Number of uses", "Name");

      foreach (var r in runners) {
        csv.Add(r.Count, r.Club);
      }

      Write(fileName, csv.Flush());
    }

    private void CreateOriginalClubReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => p.OriginalClub)
        .Select(p => new { p.FirstOrDefault().OriginalClub, Count = p.Count() })
        .OrderByDescending(p => p.Count);

      var csv = new CSV();
      csv.Add("Number of uses", "Name");

      foreach (var r in runners) {
        csv.Add(r.Count, r.OriginalClub);
      }

      Write(fileName, csv.Flush());
    }

    private void Write(string path, string content) {
      File.WriteAllText(Path.Combine(_Configuration.BasePath, path), content);
    }

    private void CreateReport(List<EventInformation> events, string fileName) {
      var buffer = new StringBuilder();

      foreach (var e in events) {
        Console.WriteLine($"Processing {e.Name}");
        buffer.AppendLine("******************************************************************");
        buffer.AppendLine("******************************************************************");
        buffer.AppendLine(e.Date.ToString(RealDateFormat));
        buffer.AppendLine(e.Organisers);
        buffer.AppendLine(e.Name);
        buffer.AppendLine(e.Source);
        foreach (var g in e.Grades.OrderBy(gr => gr.Name)) {
          string kmRateString = "N/A";
          string winningTime = "N/A";
          if (g.Length > 0) {
            var kmRate = (g.WinningTime / 60) / (g.Length / 1000) ?? 0;
            kmRateString = TimeSpan.FromMinutes(kmRate).ToString("mm\\:ss");
          }
          if (g.WinningTime.HasValue) {
            winningTime = TimeSpan.FromSeconds(g.WinningTime ?? 0).ToString("mm\\:ss");
          }
          buffer.AppendLine($"{g.Name.Pad(30)} - {g.NumberOfRunners.Pad(5)} - {g.Course.Pad(10)} - {g.Length.Pad(5)}m - {g.WinningTime}s - {winningTime} - {kmRateString} min/km");
        }
        buffer.AppendLine("+++++++++++++++++++++++++++++++++++++++++++++++++++");
        foreach (var c in e.Clubs.OrderBy(gr => gr.Name)) {
          buffer.AppendLine($"{c.Name.Pad(30)} - {c.NumberOfRunners.Pad(5)}");
        }
      }

      Write(fileName, buffer.ToString());
    }

    private void CreateRunnerReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => $"{p.FirstName} {p.LastName} {p.Card} {p.Club}")
        .Select(p => new { p.FirstOrDefault().FirstName, p.FirstOrDefault().LastName, p.FirstOrDefault().Card, p.FirstOrDefault().Club, Count = p.Count(), p.OrderByDescending(p => p.Date).FirstOrDefault().Date })
        .OrderByDescending(p => p.Count);

      var csv = new CSV();

      foreach (var r in runners) {
        csv.Add(r.Count, r.LastName, r.FirstName, r.Card, r.Club, r.Date.ToString(RealDateFormat));
      }

      Write(fileName, csv.Flush());
    }

    private void CreatePersonReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => $"{p.FirstName} {p.LastName}")
        .Select(p => new { p.FirstOrDefault().FirstName, p.FirstOrDefault().LastName, Cards = p.Select(p => p.Card).GroupBy(p => p).Select(p => new { Card = p.Key, NumberOfUses = p.Count() }).OrderByDescending(p => p.NumberOfUses) });

      var csv = new CSV();

      foreach (var r in runners.OrderBy(p => p.LastName).ThenBy(p => p.FirstName)) {
        csv.Add(r.LastName, r.FirstName, r.Cards.Sum(p => p.NumberOfUses), String.Join(",", r.Cards.Select(p => $"{p.Card}[{p.NumberOfUses}]")));
      }

      Write(fileName.Replace(".", "-sortedbyname."), csv.Flush());

      var csv2 = new CSV();

      foreach (var r in runners.OrderBy(p => p.Cards.Sum(s => s.NumberOfUses)).ThenBy(p => p.LastName).ThenBy(p => p.FirstName)) {
        csv2.Add(r.LastName, r.FirstName, r.Cards.Sum(p => p.NumberOfUses), String.Join(",", r.Cards.Select(p => $"{p.Card}[{p.NumberOfUses}]")));
      }

      Write(fileName.Replace(".", "-sortedbyruns."), csv2.Flush());
    }

    private void CreateSinglePersonReport(List<FlattendRecord> all, string fileName, Func<FlattendRecord, bool> filter) {
      var runners = all
        .Where(filter);

      var csv = new CSV();

      foreach (var r in runners) {
        csv.Add(r.LastName, r.FirstName, r.Card, r.Date.ToString(RealDateFormat), r.EventName, r.Organisers);
      }

      Write(fileName, csv.Flush());
    }

    private void CreateCardReport(List<FlattendRecord> all, string fileName) {
      var runners = all
        .GroupBy(p => $"{p.Card}")
        .Select(p => new { Card = p.Key, NumberOfUses = p.Count(), Runners = p.Select(r => new { r.FirstName, r.LastName, r.Club, r.Date }).GroupBy(f => $"{f.FirstName}{f.LastName}").OrderByDescending(p => p.Count()) })
        .OrderByDescending(p => p.NumberOfUses);

      var csv = new CSV();

      foreach (var r in runners) {
        csv.Add(r.NumberOfUses, r.Card, String.Join(",", r.Runners.Select(p => $"{p.FirstOrDefault()?.FirstName } {p.FirstOrDefault()?.LastName }[{p.Count()}]")));
      }

      Write(fileName, csv.Flush());
    }
    private void CreateCardPredictReport(List<FlattendRecord> all, string fileName, Func<List<CardPredictRecord>, List<CardPredictRecord>> Sort, Func<List<CardPredictRecord>, List<CardPredictRecord>> filter) {
      var runners = Sort(all
        .Where(p => !String.IsNullOrWhiteSpace(p.Club))
        .GroupBy(p => $"{p.Card}")
        .Select(p => {
          var owner = GetOwner(p.ToList());

          List<string> clubs = new List<string>();

          if (owner != null) {
            clubs = p.Where(p => p.FirstName == owner.FirstName && p.LastName == owner.LastName).Select(p => p.Club).ToList();
          }

          return new CardPredictRecord { Card = p.Key, NumberOfUses = p.Count(), FirstName = owner?.FirstName, LastName = owner?.LastName, Club = owner?.Club, Club2 = owner?.Club2, Date = owner?.Date, Clubs = clubs };
        }).ToList());

      if (filter != null) {
        runners = filter(runners);
      }

      WriteJson(fileName.Replace(".csv", ".json"), runners);

      var csv = new CSV();

      csv.Add("NumberOfUses", "Card", "First name", "Lastname", "Club", "Changed", "Most common club 2", $"{_Configuration.MinRecords} records Date", "Clubs");

      foreach (var r in runners) {
        csv.Add(r.NumberOfUses.ToString(), r.Card, r.FirstName, r.LastName, r.Club, r.Club == r.Club2 ? "" : "*", r.Club2, r.Date?.ToString(RealDateFormat), String.Join(", ", r.Clubs.GroupBy(p => p).Select(p => new { Name = p.Key, Count = p.Count() }).OrderByDescending(p => p.Count).Select(p => $"{ p.Name}[{p.Count}]")));
      }

      Write(fileName, csv.Flush());
    }

    private void WriteJson<T>(string path, T instance) {
      File.WriteAllText(Path.Combine(_Configuration.BasePath, path), JsonSerializer.Serialize(instance, Helper.GetJsonOptions()));
    }

    private FlattendRecord GetOwner(List<FlattendRecord> records) {
      var person = records
        .OrderByDescending(p => p.Date)
        .Where(p => !String.IsNullOrWhiteSpace(p.Club))
        .GroupBy(g => $"{g.FirstName} {g.LastName}")
        .Where(p => p.Count() >= _Configuration.MinRecords)
        .Select(p => new { Person = p.ToList()[_Configuration.MinRecords - 1] })
        .OrderByDescending(p => p.Person.Date)
        .Select(p => p.Person)
        .FirstOrDefault();

      if (person == null) {
        return null;
      }

      // && !String.IsNullOrWhiteSpace(person.Club) && p.Date >= person.Date
      var mostUsedClub = records.Where(p => p.FirstName == person.FirstName && p.LastName == person.LastName && !String.IsNullOrWhiteSpace(person.Club)).Select(p => p.Club).GroupBy(p => p).OrderByDescending(p => p.Count());

      var last = new Dictionary<string, int>();

      foreach (var item in records.Where(p => p.FirstName == person.FirstName && p.LastName == person.LastName && !String.IsNullOrWhiteSpace(person.Club))) {
        if (!last.ContainsKey(item.Club)) {
          last.Add(item.Club, 0);
        }
        last[item.Club] = last[item.Club] + 1;
        if (last[item.Club] >= _Configuration.MinRecords) {
          person.Club = item.Club;
        }
      }

      person.Club2 = mostUsedClub.FirstOrDefault().Key;
      return person;
    }

    private List<EventInformation> ProcessEvents(bool force) {
      string statsPath = Path.Combine(_Configuration.FileBasePath, "stats.json");

      var options = Helper.GetJsonOptions();

      var events = new List<EventInformation>();

      if (!force) {
        try {
          return JsonSerializer.Deserialize<List<EventInformation>>(File.ReadAllText(statsPath), options);
        } catch { }
      }

      var files = Directory.GetFiles(Path.Combine(_Configuration.BasePath, "out", "patched")); //.Take(1000);

      foreach (var file in files) {
        Console.WriteLine($"Processing {file}.");
        var result = Helper.LoadInstanceFromXml<ResultList>(file);

        var organisers = String.Join(", ", result.Event.Organiser.Select(p => p.Name));

        //if (wellingtonEvent.Any(o => organisers == o)) {
        var grades = result.ClassResult.Select(p => {
          double? winningTime = null;

          if (p.PersonResult != null) {
            winningTime = p.PersonResult.Where(p => p.Result?[0]?.Status == ResultStatus.OK && (p.Result?[0]?.TimeSpecified ?? false)).Select(p => p.Result[0].Time).OrderBy(p => p).FirstOrDefault();
          }

          return new GradeInformation {
            Course = p.Course?[0]?.Name,
            Length = p.Course?[0]?.Length,
            Name = p.Class?.Name,
            WinningTime = winningTime,
            NumberOfRunners = p.PersonResult?.Length
          };
        }).ToList();

        var clubs = result.ClassResult.Where(p => p.PersonResult != null).SelectMany(p => p?.PersonResult).Select(p => p?.Organisation?.Name).Where(p => !String.IsNullOrWhiteSpace(p)).GroupBy(p => p).Select(p => new ClubInformation {
          Name = p.Key,
          NumberOfRunners = p.Count()
        }).ToList();

        var runners = result.ClassResult.Where(p => p.PersonResult != null).SelectMany(p => p?.PersonResult, (c, p) => new Runner {
          LastName = p.Person?.Name?.Family,
          FirstName = p.Person?.Name?.Given,
          Club = p.Organisation?.Name,
          Card = p.Result?[0]?.ControlCard?[0]?.Value,
          Class = c.Class.Name
        }).ToList();

        events.Add(new EventInformation {
          Name = result.Event.Name,
          Date = result.Event.StartTime.Date,
          Time = result.Event.StartTime.TimeSpecified ? result.Event.StartTime.Time : (DateTime?)null,
          Organisers = organisers,
          Grades = grades,
          Clubs = clubs,
          Runners = runners,
          Source = new FileInfo(file).Name,
        });
      }

      File.WriteAllText(Path.Combine(_Configuration.FileBasePath, "stats.json"), JsonSerializer.Serialize(events, options));

      return events;
    }
  }
}
