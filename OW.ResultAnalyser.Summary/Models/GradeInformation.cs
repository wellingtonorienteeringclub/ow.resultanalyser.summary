﻿namespace OW.ResultAnalyser.Summary {
  public class GradeInformation {
    public string Name { get; set; }
    public string Course { get; set; }
    public double? Length { get; set; }
    public double? WinningTime { get; set; }
    public int? NumberOfRunners { get; internal set; }
  }
}
