﻿using System.Collections.Generic;

namespace OW.ResultAnalyser.Summary {
  public class ClubMapping {
    public string Name { get; set; }

    public List<string> Aliases { get; set; }
  }
}