﻿using System;
using System.Text;

namespace OW.ResultAnalyser.Summary {
  public class CSV {
    public StringBuilder _Buffer;

    public CSV() {
      _Buffer = new StringBuilder();
    }

    public void Add(params object[] list) {
      _Buffer.AppendLine("\"" + String.Join("\",\"", list) + "\"");
    }

    public string Flush() {
      return _Buffer.ToString();
    }
  }
}
