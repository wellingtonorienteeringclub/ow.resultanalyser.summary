﻿namespace OW.ResultAnalyser.Summary {
  public class Person {
    public string FirstName { get; set; }
    public string LastName { get; set; }
  }
}