﻿namespace OW.ResultAnalyser.Summary {
  public class ClubInformation {
    public string Name { get; set; }
    public int NumberOfRunners { get; set; }
  }
}
