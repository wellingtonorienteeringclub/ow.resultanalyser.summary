﻿namespace OW.ResultAnalyser.Summary {
  public class Runner {
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string Club { get; set; }
    public string Card { get; set; }
    public string Class { get; set; }
  }
}
