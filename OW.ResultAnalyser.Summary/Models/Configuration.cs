﻿using System.Collections.Generic;

namespace OW.ResultAnalyser.Summary {
  public class Configuration {
    public string FileBasePath { get; set; }

    public string BasePath { get; set; }

    public int Year { get; set; }

    public int MinRecords { get; set; }

    public int RentalUsageLimit { get; set; }

    public int RentalPersonLimit { get; set; }

    public bool FilterOrganisers { get; set; }

    public List<string> Organisers { get; set; }

    public bool FilterSportIdents { get; set; }

    public List<int> SportIdents { get; set; }

    public bool FilterClubs { get; set; }

    public List<string> Clubs { get; set; }

    public List<Person> Persons { get; set; }

    public bool FilterOriginalClubs { get; set; }

    public List<string> OriginalClubs { get; set; }
  }
}