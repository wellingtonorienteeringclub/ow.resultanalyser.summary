﻿using System;

namespace OW.ResultAnalyser.Summary {
  public class FlattendRecord {
    public DateTime Date { get; set; }
    public string EventName { get; set; }
    public string Organisers { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int? Card { get; set; }
    public string Club { get; set; }
    public string Club2 { get; set; }
    public string Class { get; set; }
    public string OriginalClub { get; set; }

    //public string KMRate { get; set; }
    //public string Placing { get; set; }
    //public string Status { get; set; }
    //public string Time { get; set; }
    //public string TimeBehind { get; set; }
  }
}
