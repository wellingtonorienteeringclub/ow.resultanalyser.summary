﻿using System;
using System.Collections.Generic;

namespace OW.ResultAnalyser.Summary {
  public class EventInformation {
    public string Name { get; set; }
    public DateTime Date { get; set; }
    public DateTime? Time { get; set; }
    public string Organisers { get; set; }
    public List<GradeInformation> Grades { get; set; }
    public string Source { get; set; }
    public List<ClubInformation> Clubs { get; set; }
    public List<Runner> Runners { get; set; }
  }
}
