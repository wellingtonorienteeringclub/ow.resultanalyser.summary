﻿using System;
using System.Collections.Generic;

namespace OW.ResultAnalyser.Summary {
  internal class CardPredictRecord {
    public string Card { get; set; }
    public int NumberOfUses { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Club { get; set; }
    public DateTime? Date { get; set; }
    public List<string> Clubs { get; set; }
    public string Club2 { get; set; }
  }
}