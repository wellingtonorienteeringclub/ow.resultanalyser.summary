Run with ``Visual Studio 2019`` or ``VS Code``.

Make sure you have the ``dotnet core 3.1`` framework installed.

Create a folder and place the ``clubs.json`` and ``stats.json`` files in newly created folder. 
Create a another folder for the report files.
Update the path to the folders in the ``configuration.json``
Set the rest of the configuration as needed...