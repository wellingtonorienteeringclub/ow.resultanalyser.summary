﻿using System;
using System.IO;
using System.Text.Json;
using System.Xml.Serialization;

namespace OW.ResultAnalyser.Summary {
  public static class Helper {
    public static JsonSerializerOptions GetJsonOptions() {
      return new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, WriteIndented = true };
    }

    public static string Pad(this string input, int length) {
      if (String.IsNullOrWhiteSpace(input)) {
        input = "";
      }

      return input.PadRight(length, ' ');
    }

    public static string Pad<T>(this Nullable<T> input, int length) where T : struct {
      var value = input == null ? "" : input.ToString();
      return value.Pad(length);
    }

    public static string Pad(this int input, int length) {
      return input.ToString().Pad(length);
    }

    public static T LoadInstanceFromXml<T>(string path) where T : class {
      using (FileStream fileStream = new FileStream(path, FileMode.Open)) {
        return new XmlSerializer(typeof(T)).Deserialize(fileStream) as T;
      }
    }

    public static string GetCardVersion(this int? card) {
      if (!card.HasValue) {
        return null;
      }

      if (card >= 8000001 && card <= 8999999) {
        return "SIAC";
      }

      if (card >= 9000001 && card <= 9999999) {
        return "11";
      }

      if (card >= 7000001 && card <= 7999999) {
        return "10";
      }

      if (card >= 1000001 && card <= 1999999) {
        return "9";
      }

      if (card >= 2000001 && card <= 2999999) {
        return "8";
      }

      if (card >= 500000 && card <= 999999) {
        return "6";
      }

      if (card >= 1 && card <= 499999) {
        return "5";
      }

      return "?";
    }

    public static int? CovertToInt(this string card) {
      if (String.IsNullOrWhiteSpace(card)) {
        return null;
      }

      if (Int32.TryParse(card, out int number)) {
        return number;
      }

      return null;
    }
  }
}
