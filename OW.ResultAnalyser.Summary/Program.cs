﻿using System.IO;
using System.Text.Json;

namespace OW.ResultAnalyser.Summary {
  class Program {
    static void Main(string[] args) {
      var configuration = JsonSerializer.Deserialize<Configuration>(File.ReadAllText("configuration.json"), Helper.GetJsonOptions());
      new Worker(configuration).Work();
    }
  }
}
